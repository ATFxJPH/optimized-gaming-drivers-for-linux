pkgname=mesa-git
pkgdesc="Open-source OpenGL & Vulkan drivers 64-bit, git version"
pkgver=latest
pkgrel=1
arch=('x86_64')

makedepends=('git' 'bison' 'cmake' 'expat' 'flex' 'glslang' 'libclc' 'meson' 'libdrm' 'libelf' 'libglvnd' 'libunwind' 'libva' 'libvdpau' 'libx11' 
'libxcb' 'libxext' 'libxfixes' 'libxrandr' 'libxshmfence' 'lm_sensors' 'llvm' 'llvm-libs' 'clang' 'spirv-llvm-translator' 'spirv-tools' 
'pkgconf' 'python-mako' 'python-build' 'python-ply' 'rust' 'rust-bindgen' 'wayland' 'wayland-protocols' 'xorgproto' 'zlib' 'zstd')

optdepends=('opengl-man-pages')

conflicts=('mesa' 'opencl-clover-mesa' 'opencl-rusticl-mesa' 'libva-mesa-driver' 'vulkan-mesa-layers' 
'mesa-vdpau' 'vulkan-intel' 'vulkan-radeon' 'vulkan-nouveau' 'vulkan-swrast' 'vulkan-virtio')

provides=('mesa' 'mesa-libgl' 'libglvnd' 'opengl-driver' 'vulkan-driver')

url="https://www.mesa3d.org"
license=('custom')
source=('mesa::git+https://gitlab.freedesktop.org/mesa/mesa.git#branch=main' 'LICENSE')
b2sums=('SKIP' 'SKIP')

pkgver() {
    cd mesa
    local _ver
    read -r _ver <VERSION
    echo ${_ver/-/_}.$(git rev-list --count HEAD).$(git rev-parse --short HEAD).${_patchver}
}

prepare() {
    if [  -d _build ]; then
        rm -rf _build
    fi
    wget "https://gitlab.com/ATFxJPH/linux-drivers-unleashed/-/raw/main/Custom%20Patches/00-radv-defaults.conf?ref_type=heads&inline=true" -O 00-radv-defaults.conf
    cp -f 00-radv-defaults.conf "${srcdir}/mesa/src/util"/
}

build () {
    meson setup mesa _build \
       -D b_lto=true \
       -D platforms=x11,wayland \
       -D osmesa=false \
       -D shader-cache=disabled \
       -D shader-cache-default=false \
       -D gallium-opencl=icd \
       -D gallium-drivers=radeonsi \
       -D vulkan-drivers=amd \
       -D vulkan-beta=false \
       -D vulkan-layers=device-select,overlay \
       -D amd-use-llvm=false \
       -D glx=dri \
       -D egl=enabled \
       -D glvnd=enabled \
       -D valgrind=disabled \
       -D video-codecs=all \
       -D zstd=enabled \
       -D prefix=/usr \
    
   meson configure --no-pager _build
   
    ninja -C _build
}

package() {
    DESTDIR="${pkgdir}" ninja -C _build install
    
    install -m644 -Dt "${pkgdir}/usr/share/licenses/${pkgname}" "${srcdir}/LICENSE"
}