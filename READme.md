## Getting started
Open Terminal within x32 run `makepkg -s --nobuild` then `makepkg -s --noextract` repeat within x64

for git make sure the same version for x32 & x64 is being compiled.

For meson use `makepkg -s --nocheck` to compile normally, use `makepkg -s --noextract` to compile with meson build test.

Regardless of the Build Variant you will have my Main Optimizations

## Installation
Run the Extracted Packages with your Package Manager to Install

## Contributing

## Authors and acknowledgment
ATFx

## License
For open source projects

## Rust Disclaimer
Rust separates its functions into a https://crates.io/ Rust Repo unlike other Languages. For devs & or maintainers

that causes having to maintain multiple crates versions to utilize new features in any for any project utilizing.